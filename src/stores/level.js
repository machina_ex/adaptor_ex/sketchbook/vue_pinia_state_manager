import { defineStore } from "pinia";

export const useLevelStore = defineStore({
  // name of the store
  // it is used in devtools and allows restoring state
  id: "level",
  // a function that returns a fresh state
  state: () => ({
    raw: {},
    name: "",
    states: []
  }),
  // optional getters
  getters: {
    doubleCount() {
      return this.counter * 2;
    }
  },
  // optional actions
  actions: {
    init(level) {
      this.raw = level;
      this.name = level.name || "";
      this.states = level.chapter.chapter || [];
    },
    rename(name) {
      this.name = name;
    },
    remove(id) {
      // TODO
      console.log(`remove state ${id} from this.states`);
    },
    update(newState) {
      console.log(newState);
      // TODO
      let id = newState.view.id;
      let i = this.states.findIndex(item => item.view.id == id);
      this.states[i] = newState;
    }
  }
});
