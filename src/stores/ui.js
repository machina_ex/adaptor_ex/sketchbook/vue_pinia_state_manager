import { defineStore } from "pinia";

export const useUiStore = defineStore({
  id: "ui",
  state: () => ({
    sidebar: {
      open: false,
      mode: "author"
    }
  }),
  getters: {
    sidebarOpen() {
      return this.sidebar.open;
    }
  },
  actions: {
    openSidebar() {
      this.sidebar.open = true;
    },
    closeSidebar() {
      this.sidebar.open = false;
    },
    setMode(mode) {
      this.sidebar.mode = mode;
    }
  }
});
